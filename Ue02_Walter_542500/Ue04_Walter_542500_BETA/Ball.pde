/*=============================================================================================================================*/
/* Ball */
class Ball {
  float x;
  float y;
  float diameter;
  float rad;
  float weight;
  color c;
  float deltaTime = 0;
  float time;
  float time0 = 0;
  float vY0 = 0;
  float vY;
  float vX0 = 0;
  float x0;
  float y0;
  Ball (float _x, float _y, float _diameter, float _weight, color _c) {
    x = _x;
    y = _y;
    diameter = _diameter;
    weight = _weight;
    c = _c;
    rad = diameter / 2;
  }

  void display () {

    ellipseMode(CENTER);
    fill(c);
    ellipse (calcScreenPositionX((x)), 
      calcScreenPositionY(scaleMToPixels(y)), 
      scaleMToPixels(diameter), 
      scaleMToPixels(diameter));
  }
  /*
  *
   */
  void move () {
    time0 = millis();
    calcPositionY();
    display();
  }
  void calcPositionY () {
    deltaTime = (millis() - time0) / SCALE_FACTOR_TIME;
    x = x0 + vX0 * deltaTime;
    y = y0 + vY0 *deltaTime - (g*(deltaTime*deltaTime))/2;
  }
  /*
  *Scales the unit M to pixels.
   */
  float scaleMToPixels (float _m) {
    return _m * SCALE_FACTOR;
  }

  float getX () {
    return x;
  }
  float getY () {
    return y;
  }
  float getDiameter () {
    return diameter;
  }

  color getC () {
    return c;
  }
}