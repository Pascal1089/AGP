class CannonBall extends Ball { //<>// //<>//
  float pouderCharge;

  CannonBall(float _x, float _y, float _diameter, float _weight, color _c, float _pouderCharge) {
    super (_x, _y, _diameter, _weight, _c);
    x0 = _x;
    y0 = _y;
    pouderCharge = _pouderCharge;
 
  }
  void calcPosition () {
    deltaTime = (millis() - time0) / SCALE_FACTOR_TIME;
    x = x0 + vX0 * deltaTime;
    y = y0 + vY0 *deltaTime - (g*(deltaTime*deltaTime))/2;
    println(x + " " + y);
  }
  void move () {
    time0 = millis();
    display();
  }
  void setVx0 (float v ) {
    vX0 = v;
  }
  void setVy0 (float v) {
    vY0 = v;
  }
  void display () {
    calcPosition ();
    if (hitWall(this, wallRight) || hitWall(this, wallLeft)) {
     
      move();
    } 
    if (y - rad < 0) {
      y = 0 + rad;
      //DONE!
    }
    stroke(color(0));
    ellipseMode(CENTER);
    fill(c);
    ellipse (calcScreenPositionX(scaleMToPixels(x)), 
      calcScreenPositionY(scaleMToPixels(y)), 
      scaleMToPixels(diameter), 
      scaleMToPixels(diameter));
  }

  boolean hitBomb() {
    if (hitBall(x, y, diameter / 2, bigBall.x, bigBall.y, bigBall.diameter/2)) {
      return true;
    }
    return false;
  }
}