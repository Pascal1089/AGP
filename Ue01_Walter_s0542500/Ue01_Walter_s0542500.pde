/*
Autor: Pascal Walter, 542500
Datum: 2015-10-08

*/

/**
* Erstellt eine "Canvas".
*/  

float width = 800;
int height = 400;

void setup() {
  size(800,400);
  background(255,255,255);
}

void draw() {

   rectMode(CENTER);
  //left-score-label
 fill(0,255,0);
  rect(150,45,100,50,25);
 rect(150,height-45,100,50,25);  
  //right-score-label;
  fill(255,0,0);
  rect(width-150,45,100,50,25);
 //fire-button-right
  rect(width-150,height-45,100,50,25);
  //Ground
  rectMode(CORNER);
  fill(0);
  rect(0,300,width,10);
  //TEXT
  fill(0);
  textSize(30);
  text("Score",110,50);
 text("Score",width-190,50);
 text("Fire",110,height-40);
 text("Fire",width-190,height-40);
  
  // Wand-left
  fill(155,155,0);
  rect(180,305,25,-70);
  //Wand-right
  rect(width-190,305,25,-70);
  
  //Canon-left
  fill(0);
  pushMatrix();
  translate(10,290);
  rotate(radians(-40));
  rect(0,0,100,30); 
  popMatrix();
  fill(255);
  ellipse(35,290,20,20);
  //Canon-right
  fill(0);
  pushMatrix();
  translate(width-25,310);
  rotate(radians(-140));
  rect(0,0,100,30);
  popMatrix();
   fill(255);
  ellipse(width-35,290,20,20);
  
  //Bomb small
  fill(0);
  ellipse(width/2,height/2,20,20);
  fill(150,150,255);
  ellipse(width/2-30,275,50,50);
  
  
}