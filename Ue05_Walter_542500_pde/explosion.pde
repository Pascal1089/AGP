class Explosion {
  float x;
  float y;
  float diameter;
  float diameterFactor = 0;

  Explosion (float _x, float _y, float _diameter) {
    x = _x;
    y = _y;
    diameter = _diameter;
  }

  void display() {

    ellipseMode(CENTER);
    fill(255, 124, 0 + diameterFactor );
    ellipse(calcScreenPositionX(scaleMToPixels(x)), calcScreenPositionY(scaleMToPixels(y)), scaleMToPixels(diameter + diameterFactor), scaleMToPixels(diameter + diameterFactor));
    if (diameterFactor > 4) {
      fill(#FFEB03);
      ellipse(calcScreenPositionX(scaleMToPixels(x)), calcScreenPositionY(scaleMToPixels(y)), scaleMToPixels(diameter + diameterFactor ), scaleMToPixels(diameter +  diameterFactor ));
    }
    diameterFactor ++;
  }
}