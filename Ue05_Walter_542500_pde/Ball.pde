/*=============================================================================================================================*///<>// //<>//
/* Ball */
class Ball {
  float x;
  float y;
  float diameter;
  float rad;
  float weight;
  color c;
  float time;
  float time0 = 0;
  float vY0 = 0;
  float vX0 = 0;
  float x0;
  float y0;
  float FN;
  float FNR;
  float angle;
  float pouderCharge;
  boolean hitW;
  float xBefore;
  float yBefore;
  boolean noVelocity = false;
  float vX = 0;
  float vY = 0;
  float t = 0;
 // float timeDelta = 1 / 30;

  boolean isOnGroundCheck = false;
  Ball (float _x, float _y, float _diameter, float _weight, color _c) {
    x = _x;
    y = _y;
    x0 = _x;
    y0 = _y;
    weight = (4/3)*PI*(_diameter / 2) * (_diameter / 2) * 0.000787;
    diameter = _diameter;
    weight = _weight;
    c = _c;
    rad = diameter / 2;
    FN = weight * g;
    FNR =  weight * g * coefizient;
  }

  void display () {
    xBefore = x;
    yBefore = y;
    
      calcOnGround();
   if(y-rad <0) {
   y = rad;
   }

    ellipseMode(CENTER);
    fill(c);
    ellipse (calcScreenPositionX(scaleMToPixels(x)), 
      calcScreenPositionY(scaleMToPixels(y)), 
      scaleMToPixels(diameter), 
      scaleMToPixels(diameter));
  }
  /*
   *Starts the movement of the cannon ball.
   */
  void move () {
    display();
    noVelocity = false;
  }
  void calcPosition () {
    t = t + timeDelta;
    //x = x0 + vX0 * t;
    x = x + vX * timeDelta;

    //y = y0 + vY0 *deltaTime - (g*(deltaTime*deltaTime))/2;
    vY = vY - g * timeDelta;
    y = y + vY * timeDelta;
  }

  /**
   *Calculates vX0 and vY0 with an angle and a force.
   */
  void calculateVxVy(float _angle, float _pouderCharge) {
    angle = _angle;
    vX = cos(radians(- _angle)) * _pouderCharge;
    vY = sin(radians(- _angle)) * _pouderCharge;
    vX0 = vX;
    vY0 = vY;
  }
  /**
   *Changes direction from the ball if ball hits obsticle.
   */
  void changeDirection() {
    calculateAngle();
    vX = cos(radians(angle)) * pouderCharge;
    vX0 = -vX0;
  }
  void changeDirectionOnGround() {
    vX = -vX;
    vX0 = vX;
  }
  /**x
   *Calculates the x coordinate if ball hits ground.
   */
  void calcOnGround() {
    t = t + 1 / 30;
    y = rad;
    if (vX != 0) {
      if (vX0 > 0) {
        if ( 0 < vX ) {
          vX = vX - (0.5 * coefizient * g  / 30);
          x = x + vX  / 30 ;
          //  println("left");
        } else {
          x0 = x;
          noVelocity = true;
        }
      } else {
        if (0 > vX ) {
          vX = vX + (0.5 * coefizient * g / 30); 
          x = x + vX /  30 ;
          //println("right");
        } else {
          x0 = x;
          noVelocity = true;
        }
      }
    }
  }
  /**
   *Checks if Ball is on the ground.
   */
  boolean isOnGround() {
    if (this.y - this.diameter / 2 <= 0) {
      if (!isOnGroundCheck) {
        resetTime0();
      }
      return true;
    }
    return false;
  }

  void resetTime0 () {
    time0 = millis();
  }

  /*
  *Scales the unit M to pixels.
   */
  float scaleMToPixels (float _m) {
    return _m * SCALE_FACTOR;
  }

  float getX () {
    return x;
  }

  float getY () {
    return y;
  }

  float getDiameter () {
    return diameter;
  }

  color getC () {
    return c;
  }

  void calculateAngle() {
    float aX = xBefore - x;
    float aY = yBefore - y;
    angle = cos((aX*1)/(sqrt(aX*aX+aY*aY)+1));
  }
}