class CannonBall extends Ball { //<>// //<>//
  float pouderCharge;
  float angle;

  CannonBall(float _x, float _y, float _diameter, float _weight, color _c, float _pouderCharge) {
    super (_x, _y, _diameter, _weight, _c);
    x0 = _x;
    y0 = _y;
    pouderCharge = _pouderCharge;
  }

  void changeDirection() {
    if (x0 < 0) {
      calculateVxVy(180 - angle * 2, -pouderCharge);
    } else {
      calculateVxVy(180 - angle * 2, pouderCharge);
    }
  }
  void setVx0 (float v ) {
    vX0 = v;
  }
  void setVy0 (float v) {
    vY0 = v;
  }

    void display () {
    xBefore = x;
    yBefore = y;
    if (!isOnGroundCheck) {
      calcPosition();
    }
    if (isOnGround()) {  
      if (!isOnGroundCheck) {
        vX0 = vX;
        x0 = x;
        y0 = y;
        vY = 0;
        isOnGroundCheck = true;
      }
    }
    if (hitWall(this, wallRight) || hitWall(this, wallLeft)) {
      hitW = true;
    }
    if (hitW) {
      x0 = x;
      y0 = y;
      if (isOnGroundCheck) {
        changeDirectionOnGround();
      } else {
        changeDirection();
      }
      hitW = false;
    }
    if (isOnGroundCheck) {
      calcOnGround();
      y = diameter / 2 ;
    }

    ellipseMode(CENTER);
    fill(c);
    ellipse (calcScreenPositionX(scaleMToPixels(x)), 
      calcScreenPositionY(scaleMToPixels(y)), 
      scaleMToPixels(diameter), 
      scaleMToPixels(diameter));
  }
}