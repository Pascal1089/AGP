class Stone {
  float x;
  float y;
  float diameter;
  float rad;
  float weight;
  float deltaTime = 0;
  float time;
  float time0 = millis();
  float vY0 = 0;
  float vY;
  float vX0 = 10;
  float x0;
  float y0;
  
  Stone (float _x, float _y, float _diameter, float _weight) {
    x = _x;
    y = _y;
    diameter = _diameter;
    weight = _weight;
    rad = _diameter / 2;
    x0=_x;
    y0 = _y;
  }
  void display () {
    
    deltaTime = (millis() - time0) / 5000;
    if(hitWall (this,re)) {
    vX0 = -vX0;
    x0 = x;
    }
    x = x0 + vX0 * deltaTime;
    // y = vY0 * (deltaTime) - ((g * pow(deltaTime, 2))/2) ;
   //y = y0 + vY0 *deltaTime + (g*(deltaTime*deltaTime))/2;
    ellipseMode (CENTER);
    fill(0);
    ellipse(x, y, diameter, diameter);
  }
  void move () {
    calcPositionY(y);
    display();
  }
  void calcPositionY (float _y ) {
    deltaTime = (millis() - time0) / 5000;
    x = x0 + vX0 * deltaTime;
    // y = vY0 * (deltaTime) - ((g * pow(deltaTime, 2))/2) ;
    y = y0 + vY0 *deltaTime - (g*(deltaTime*deltaTime))/2;
  }
}