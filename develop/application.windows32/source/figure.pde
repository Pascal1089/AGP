class Chara {
  float x;
  float y;
  float h;
  Chara (float _x, float _y, float _h) {
    x = _x;
    y = _y;
    h = _h;
  }

  void display () {
    fill(0);
    ellipse(x, h - h / 5, h / 5, h / 5 );
    line(x,  h - h / 5,  x , 4 * h / 5 );
    line(x,  3 * h / 5,  x - 10 , 0 );
  }
}