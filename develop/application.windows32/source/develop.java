import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.sound.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class develop extends PApplet {



AudioDevice device;
SoundFile[] file;
float angle; //<>//
float y;
boolean press;
static float yi0;
static float xi0;
Stone stone;
static float SCALE_FACTOR = 20;
static float g = 9.81f;
Rectangle re;
Chara ch = new Chara (width / 2, height / 2, 100 );
public void setup() {
  
  device = new AudioDevice(this, 48000, 32);
  file[0] =  new SoundFile(this, "explosion.wav");
  angle = 0;
  re = new Rectangle(width/2, 200, 100, 50);
  yi0 = height;
  xi0 = width / 2;
  press = false;
  stone = new Stone (width/2 -  50, 200, 30, 50.0f );
}

public void draw() {
  background(255);
  if (hitWall(stone, re)) {
    background(255, 0, 0);
  }
  re.display();
  stone.display();
  ch.display();
}
public void mousePressed() {
  if (mouseButton == LEFT) {
    press = true;
  }
}
public void keyPressed () {
  if (keyCode ==  UP) {
    file[0].play();

    press = true;
  }
  if (keyCode == DOWN) {
    angle += 1;
  }
  println(angle);
}

public float scaleMToPixels (float _m) {

  return _m * SCALE_FACTOR;
}

/*
*Scales the unit M to pixels.
 */
public float scalePixelsToM (float _m) {

  return _m / SCALE_FACTOR;
}

public boolean hitWallGround (Rectangle g, Stone _cb) {
  //TODO: calculate
  ///////////http://www.emanueleferonato.com/2012/03/09/algorithm-to-determine-if-a-point-is-inside-a-square-with-mathematics-no-hit-test-involved/
  float x = g.x, y = g.y, le = g.le, hei = g.hei;
  boolean hit = false;
  for (int angle = 180; angle <= 360; angle += 15) {
    float  radX =_cb.x + _cb.rad * cos (radians(angle));
    float  radY =_cb.y + _cb.rad * sin (radians(angle));
    //Conditon if point position   is smaller or bigger than position x + or - the half of the length of the object / than position y + or - half of the heigh of the object
    if (_cb.x <= x + le/2 && 
      _cb.x >= x - le/2 && 
      _cb.y >= y + hei/2 && 
      _cb.y <= y - hei/2) {
      hit = true;
    }
  }
  return hit;
}
public boolean hitWall(Stone _stone, Rectangle _re) {
  for (int angle = 180; angle <= 360; angle += 15) {
    float   x = _stone.x + _stone.rad * cos (radians(angle));
    float  y = _stone.y +  _stone.rad * sin (radians(angle));
    if (x > _re.x - _re.le/2 && x < _re.x + _re.le / 2 && y < _re.y + _re.hei / 2 && y > _re.y - _re.hei / 2) {
      return true;
    }
  }
  return false;
  // return _stone.x > _re.x - _re.le/2 && _stone.x < _re.x + _re.le / 2 && _stone.y < _re.y + _re.hei / 2 && _stone.y > _re.y - _re.hei / 2;
}

class Chara {
  float x;
  float y;
  float h;
  Chara (float _x, float _y, float _h) {
    x = _x;
    y = _y;
    h = _h;
  }

  public void display () {
    fill(0);
    ellipse(x, h - h / 5, h / 5, h / 5 );
    line(x,  h - h / 5,  x , 4 * h / 5 );
    line(x,  3 * h / 5,  x - 10 , 0 );
  }
}
class Rectangle {
  float x;
  float y;
  float hei;
  float le;
  Rectangle(float _x, float _y, float _hei, float len) {
    x = _x;
    y = _y;
    hei = _hei;
    le = len;
  }

  public void display () {
   
    rectMode(CENTER);

    rect(x, y, le, hei);
  }
}
class Stone {
  float x;
  float y;
  float diameter;
  float rad;
  float weight;
  float deltaTime = 0;
  float time;
  float time0 = millis();
  float vY0 = 0;
  float vY;
  float vX0 = 10;
  float x0;
  float y0;
  
  Stone (float _x, float _y, float _diameter, float _weight) {
    x = _x;
    y = _y;
    diameter = _diameter;
    weight = _weight;
    rad = _diameter / 2;
    x0=_x;
    y0 = _y;
  }
  public void display () {
    
    deltaTime = (millis() - time0) / 5000;
    if(hitWall (this,re)) {
    vX0 = -vX0;
    x0 = x;
    }
    x = x0 + vX0 * deltaTime;
    // y = vY0 * (deltaTime) - ((g * pow(deltaTime, 2))/2) ;
   //y = y0 + vY0 *deltaTime + (g*(deltaTime*deltaTime))/2;
    ellipseMode (CENTER);
    fill(0);
    ellipse(x, y, diameter, diameter);
  }
  public void move () {
    calcPositionY(y);
    display();
  }
  public void calcPositionY (float _y ) {
    deltaTime = (millis() - time0) / 5000;
    x = x0 + vX0 * deltaTime;
    // y = vY0 * (deltaTime) - ((g * pow(deltaTime, 2))/2) ;
    y = y0 + vY0 *deltaTime - (g*(deltaTime*deltaTime))/2;
  }
}
  public void settings() {  size(1000, 500); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--stop-color=#cccccc", "develop" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
