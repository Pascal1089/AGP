class PouderChargeRegler {
  float x = 200;
  float y = 200;
  float len = 50;
  float hei = 100;
  float diameter = len + 5;
  float rad = diameter / 2;
  float positionX = 200;
  float positionY = 200;
  float A;

  PouderChargeRegler() {
    A = calculateA();
  }

  void display() {
    rectMode(CORNER);
    rect(x, y, len, hei);
    fill(0, 255, 0);
    rect(x, y, len, hei);
    if (isInRange()) {
      fill(255, 0, 0);
    } else {
      fill(0);
    }

    rectMode(CENTER);
    rect(positionX+len/2, positionY, diameter, diameter);
  }

  boolean isInRange() {

    boolean inRange = false;
    if (mouseX <= positionX + len/2 
      && mouseX >= positionX - len/2 
      && mouseY >= positionY + hei/2 
      && mouseY <= positionY - hei/2) {
      println(mouseX + " " + mouseY);
      inRange = true;
    }
    return inRange;
  }
  float calculateA() {
    return PI * rad * rad;
  }
}