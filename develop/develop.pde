import ddf.minim.*; //<>//
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import ddf.minim.signals.*;
import ddf.minim.spi.*;
import ddf.minim.ugens.*;

import processing.sound.*;
Minim minim;
AudioPlayer player;

float angle; //<>//
float y;
boolean press;
SoundFile file;
static float yi0;
static float xi0;
Stone stone;
static float SCALE_FACTOR = 20;
static float g = 9.81;
Rectangle re;
Chara ch = new Chara (width / 2, height / 2, 100 );
PouderChargeRegler pCR = new PouderChargeRegler();
void setup() {
  size(1000, 500);
minim = new Minim(this);
player = minim.loadFile("explosion.wav");
player.play(2);
player.loop();
  angle = 0;
  re = new Rectangle(width/2, 200, 100, 50);
  yi0 = height;
  xi0 = width / 2;
  press = false;
  stone = new Stone (width/2 -  50, 200, 30, 50.0 );
}

void draw() {
pCR.display();
}
void mousePressed() {
  if (mouseButton == LEFT) {
    press = true;
  }
}
void keyPressed () {
  if (keyCode ==  UP) {
    angle -= 1;

    press = true;
  }
  if (keyCode == DOWN) {
    angle += 1;
  }
  println(angle);
}

float scaleMToPixels (float _m) {

  return _m * SCALE_FACTOR;
}

/*
*Scales the unit M to pixels.
 */
float scalePixelsToM (float _m) {

  return _m / SCALE_FACTOR;
}

boolean hitWallGround (Rectangle g, Stone _cb) {
  //TODO: calculate
  ///////////http://www.emanueleferonato.com/2012/03/09/algorithm-to-determine-if-a-point-is-inside-a-square-with-mathematics-no-hit-test-involved/
  float x = g.x, y = g.y, le = g.le, hei = g.hei;
  boolean hit = false;
  for (int angle = 180; angle <= 360; angle += 15) {
    float  radX =_cb.x + _cb.rad * cos (radians(angle));
    float  radY =_cb.y + _cb.rad * sin (radians(angle));
    //Conditon if point position   is smaller or bigger than position x + or - the half of the length of the object / than position y + or - half of the heigh of the object
    if (_cb.x <= x + le/2 && 
      _cb.x >= x - le/2 && 
      _cb.y >= y + hei/2 && 
      _cb.y <= y - hei/2) {
      hit = true;
    }
  }
  return hit;
}
boolean hitWall(Stone _stone, Rectangle _re) {
  for (int angle = 180; angle <= 360; angle += 15) {
    float   x = _stone.x + _stone.rad * cos (radians(angle));
    float  y = _stone.y +  _stone.rad * sin (radians(angle));
    if (x > _re.x - _re.le/2 && x < _re.x + _re.le / 2 && y < _re.y + _re.hei / 2 && y > _re.y - _re.hei / 2) {
      return true;
    }
  }
  return false;
  // return _stone.x > _re.x - _re.le/2 && _stone.x < _re.x + _re.le / 2 && _stone.y < _re.y + _re.hei / 2 && _stone.y > _re.y - _re.hei / 2;
}