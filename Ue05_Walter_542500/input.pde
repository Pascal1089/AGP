
void mousePressed() {
/**
*Complete mouse-input handling schould be refactored after adding a game-controller class (Player site enums).
*/
  if (mouseButton == LEFT) {

    if (playerLeft.fire.isInRange()) {
      playerLeft.cannon.shoot();
    }
    if (playerRight.fire.isInRange()) {
      playerRight.cannon.shoot();
    }
    if (playerLeft.cannonUp.isInRange()) {
      playerLeft.cannon.turnCannon(-1);
    }
    if (playerRight.cannonUp.isInRange()) {
      playerRight.cannon.turnCannon(+1);
    }
    if (playerLeft.cannonDown.isInRange()) {
      playerLeft.cannon.turnCannon(+1);
    }
    if (playerRight.cannonDown.isInRange()) {
      playerRight.cannon.turnCannon(-1);
    }
    if (playerLeft.pouderChargeMore.isInRange()) {
      playerLeft.cannon.pouderCharge += 0.5;
      playerLeft.cannon.pouderCharge = playerLeft.pouderChargeInRange(playerLeft.cannon.pouderCharge);
    } 
    if (playerLeft.pouderChargeLess.isInRange()) {

      playerLeft.cannon.pouderCharge -= 0.5;
      playerLeft.cannon.pouderCharge = playerLeft.pouderChargeInRange(playerLeft.cannon.pouderCharge);
    }
    if (playerRight.pouderChargeMore.isInRange()) {
      playerRight.cannon.pouderCharge += 0.5;
      playerRight.cannon.pouderCharge = playerRight.pouderChargeInRange(playerRight.cannon.pouderCharge);
    } 
    if (playerRight.pouderChargeLess.isInRange()) {

      playerRight.cannon.pouderCharge -= 0.5;
      playerRight.cannon.pouderCharge = playerRight.pouderChargeInRange(playerRight.cannon.pouderCharge);
    }
  }
}