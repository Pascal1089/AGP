/*=======================================================================================================================================*/
/* BUTTON CLASS*/
class Button extends Ground {


  ScoreField field;
  Cannon cannon;
  String title;
  color hoverColor;
  color c;
  PImage buttonImage ;

  Button(float _x, float _y, float _length, float _height, float _radius, color _c, String _title, int _SCALE_FACTOR, ScoreField _field, Cannon _cannon) {
    super( _x, _y, _length, _height, _radius, _c, _SCALE_FACTOR);
    title = _title;
    field = _field;
    hoverColor = c + color (125);
    c = _c;
    cannon = _cannon;
    cannon.field = this.field;
  }
  Button(float _x, float _y, float _length, float _height, float _radius, color _c, String _title, int _SCALE_FACTOR) {
    super( _x, _y, _length, _height, _radius, _c, _SCALE_FACTOR);
    title = _title;
    hoverColor = _c + color (125);
    c = _c;
  }
  /*
  *Returns true if the Button is pressed;
   */
  boolean isInRange() {
    //TODO: proof conditions.
    boolean isPressed = false;
    //Conditon if mouseX is smaller or bigger than position x + or - the half of the length of the object / than position y + or - half of the heigh of the object
    if (mouseX <= calcScreenPositionX(scaleMToPixels(x + le/2)) && 
      mouseX >= calcScreenPositionX(scaleMToPixels(x - le/2)) && 
      mouseY >= calcScreenPositionY(scaleMToPixels(y + hei/2)) && 
      mouseY <= calcScreenPositionY(scaleMToPixels(y - hei/2))) {
      isPressed = true;
    }
    return isPressed;
  }

  void display () {
    if (isInRange()) {
      super.c = hoverColor;
    } else {
      super.c = c;
    }
    super.display();
    if (buttonImage != null) {
      image(buttonImage, calcScreenPositionX(scaleMToPixels(x - le / 2)), calcScreenPositionY(scaleMToPixels(y + hei / 2)));
    }
    fill(0);
    textSize(30);
    text (title, calcScreenPositionX(scaleMToPixels(x-super.le/4)), calcScreenPositionY(scaleMToPixels(y-super.hei/3)) );
    if (field != null) {
      field.display();
    }
  }
  void loadImg(String _pathToImg) {
    buttonImage = loadImage(_pathToImg);
  }
}