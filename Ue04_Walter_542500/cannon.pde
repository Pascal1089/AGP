/*===========================================================================================================================*/ //<>//
/* CANNON CLASS */
class Cannon extends Ground {
  float angle;
  float pouderCharge; // meter per second²
  CannonBall b;
  ScoreField field;
  float startAngle; 
  float weight;
  Cannon (float _x, float _y, float _length, float _height, float _radius, color _c, int _SCALE_FACTOR, float _angle) {
    super(_x, _y, _length, _height, _radius, _c, _SCALE_FACTOR);

    pouderCharge = (pouderChargeMax + pouderChargeMin) / 2;
    angle = _angle;
    startAngle = _angle;
  }

  void display () {
    //TODO cannon should turn over a point which is not a corner or the center.
    rectMode(CENTER);
    fill(128, 64, 0);
    noStroke();
    rect(calcScreenPositionX(scaleMToPixels(x)), calcScreenPositionY(scaleMToPixels(y)), le / 2, y);
    pushMatrix();
    translate(calcScreenPositionX(scaleMToPixels(x - le / 10)), calcScreenPositionY(scaleMToPixels(y - hei/2)));
    rotate(radians(angle));
    fill(0);
    rectMode(CORNER);
    rect(0 - scaleMToPixels(le / 10), 0 - scaleMToPixels(hei / 2), scaleMToPixels(le), scaleMToPixels(hei));
    popMatrix();


    if (b != null) {
      b.display();
      if (bigBall != null) {
        if (hitBall(b.x, b.y, b.diameter / 2, bigBall.x, bigBall.y, bigBall.diameter/2)) {
          this.field.increScore();
          b = null;
        }
      }
    }
  }
  /*
  *Turns the cannon.
   */
  void turnCannon (float _angle) {
    this.angle += _angle;
    //Range for angle 0°-75°
    if (angle <= startAngle - 45) {
      angle = startAngle - 45;
    }
    if (angle > startAngle + 45) { 
      angle = startAngle + 45;
    }
  }
  void shoot () {
    //TODO: Cannon shoot cannon-ball related to angle and powder charge.

    b = new CannonBall(x + le * cos(radians(-angle)), y + le * sin(radians(-angle)), hei, 0, color(255, 200, 200), pouderCharge);
    b.calculateVxVy(angle, pouderCharge);
    b.move();
  }
}