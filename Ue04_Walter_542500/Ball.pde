/*=============================================================================================================================*/
/* Ball */
class Ball {
  float x;
  float y;
  float diameter;
  float rad;
  float weight;
  color c;
  float deltaTime = 0;
  float time;
  float time0 = 0;
  float vY0 = 0;
  float vY;
  float vX0 = 0;
  float x0;
  float y0;
  float FN;
  float FNR;
  float angle;
  float pouderCharge;
  Ball (float _x, float _y, float _diameter, float _weight, color _c) {
    x = _x;
    y = _y;
    weight = (4/3)*PI*(_diameter / 2) * (_diameter / 2) * 7;
    diameter = _diameter;
    weight = _weight;
    c = _c;
    rad = diameter / 2;
    FN = weight * g;
    FNR =  weight * g * coefizient;
  }

  void display () {
    ellipseMode(CENTER);
    fill(c);
    ellipse (calcScreenPositionX((x)), 
      calcScreenPositionY(scaleMToPixels(y)), 
      scaleMToPixels(diameter), 
      scaleMToPixels(diameter));
  }
  /*
   *
   */
  void move () {
    time0 = millis();
    display();
  }
  void calcPosition () {
    deltaTime = (millis() - time0) / SCALE_FACTOR_TIME;
    x = x0 + vX0 * deltaTime ;
    y = y0 + vY0 *deltaTime - (g*(deltaTime*deltaTime))/2;
    if (vX0 < 0) {
      vX0 = 0;
    }
  }

  /*
  *Scales the unit M to pixels.
   */
  float scaleMToPixels (float _m) {
    return _m * SCALE_FACTOR;
  }

  float getX () {
    return x;
  }
  float getY () {
    return y;
  }
  float getDiameter () {
    return diameter;
  }

  color getC () {
    return c;
  }

  void calculateVxVy(float _angle, float _pouderCharge) {
    angle = _angle;
    pouderCharge = _pouderCharge;

    vX0 = cos(radians(- _angle)) * _pouderCharge;
    vY0 = sin(radians(- _angle)) * _pouderCharge;
  }

  void changeDirection() {
    vX0 = cos(radians(angle)) * pouderCharge;
    vY0 = sin(radians(angle)) * pouderCharge;
  }
  /**
   *Calculates the x coordinate if ball hits ground.
   */
  void calcOnGround() {
    deltaTime = (millis() - time0) / SCALE_FACTOR_TIME;
    x = x0 + vX0 * deltaTime - (FN * deltaTime * deltaTime) / 2;
  }

  boolean isOnGround(Ball b) {
    return b.y - b.diameter / 2 <= 0;
  }
}