/*=======================================================================================================================================*/
/* SCOREFIELD CLASS */
class ScoreField extends ButtonAndFields {
  int score;  
  float c ;
  ScoreField (float _x, float _y, float _length, float _height, float _radius, color _c, String _title, float _angle, int _SCALE_FACTOR) {
    super( _x, _y, _length, _height, _radius, _c, _title, _angle, _SCALE_FACTOR);
    score = 0;
    c = _c;
  }
  void display () {
    super.display();
    fill(0);
    textSize(30);
    text (title, calcScreenPositionX(scaleMToPixels(x-super.le/4)), calcScreenPositionY(scaleMToPixels(y-super.hei/3)) );
  }
  /*
  *Increments the pressed score-field.
   */
  void increScore () {
    this.score ++;
    this.title = this.score + "";
  }
  /*
  *Resets the Scorefield to zero.
   */
  void resetScoreField () {
    this.title = "0";
    this.score = 0;
  }
}