/*Calculations for the game*///<>//
/**
 *Scales the unit M to pixels.
 *@param
 *@return
 */
float scaleMToPixels (float _m) {

  return _m * SCALE_FACTOR;
}

/**
 *Scales the unit M to pixels.
 *@param
 *@return
 */
float scalePixelsToM (float _m) {

  return _m / SCALE_FACTOR;
}

/**
 *Calculates the X-position in machine-coordinates.
 *@param x karthesian coordinate from object
 *@return x i coordinate 
 */
float calcScreenPositionX (float _x) {
  return _x+Xi0;
}

/*
 *Calculates the Y-position in machine-coordinates
 *@param
 *@return
 */
float calcScreenPositionY (float _y) {
  return -_y+Yi0;
}
/**
 *Checks if a ball hits a wall.
 */
boolean hitWall(Ball _stone, Ground _re) {
  for (int angle = 180; angle <= 360; angle += 15) {
    float  x = _stone.x + _stone.rad * cos (radians(angle));
    float  y = _stone.y +  _stone.rad * sin (radians(angle));
    if (x > _re.x - _re.le/2 && x < _re.x + _re.le / 2 && y < _re.y + _re.hei / 2 && y > _re.y - _re.hei / 2) {
      return true;
    }
  }
  return false;
  // return _stone.x > _re.x - _re.le/2 && _stone.x < _re.x + _re.le / 2 && _stone.y < _re.y + _re.hei / 2 && _stone.y > _re.y - _re.hei / 2;
}

/**
 *Checks if a ball hits another ball.
 */
boolean hitBall(float _xBallA, float _yBallA, float _radiusBallA, float _xBallB, float _yBallB, float _radiusBallB) {
  //Compares the quadratic relation between the length of the radius of both balls and the distance between the two balls.
  return _radiusBallA * _radiusBallA + _radiusBallB * _radiusBallB > (_xBallA - _xBallB) * (_xBallA - _xBallB) + (_yBallA -  _yBallB) * (_yBallA -  _yBallB);
}