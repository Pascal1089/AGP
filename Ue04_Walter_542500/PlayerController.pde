/*=======================================================================================================================================*/
/*Class PlayerController*/
class PlayerController {
  ScoreField field;
  Button fire;
  Cannon cannon;
  Button cannonUp;
  Button cannonDown;
  Button pouderChargeMore;
  Button pouderChargeLess;
  PouderChargeLabel clabel;
  Ground[] objects;
  float pouderCharge = pouderChargeMax + pouderChargeMin / 2;
  playerSite site;
  PlayerController(ScoreField _field, Button _button, Cannon _cannon, playerSite _site) {//, Button _cannonUp, Button _cannonDown, Button _pouderChargeMore, Button _pouderChargeLess) {
    site = _site;
    field = _field;
    fire = _button;
    cannon = _cannon;
    float cux, pdx, cuY, cdy;
    float cuheilen = fire.hei / 2;
    float offset = 2;
    cuY = fire.y + 0.1 + cuheilen / 2 ;
    cdy = fire.y - 0.1 - cuheilen / 2 ;
    if (fire.x > 0) {
      cux = fire.x  - fire.le / 2 - offset;
      pdx = fire.x - 0.1 - fire.le / 2 - cuheilen - offset;
    } else {
      cux = fire.x  + fire.le / 2 + offset;
      pdx = fire.x + 0.1 + fire.le / 2 + cuheilen + offset;
    }

    clabel = new PouderChargeLabel(pdx + cuheilen / 2, cuY+cuheilen/2, cuheilen, cuheilen + cuheilen, pouderChargeMax, pouderChargeMin);
    clabel.setCharge(cannon.pouderCharge);
    cannonUp = new Button (cux, cuY, cuheilen, cuheilen, 0, fire.c, "", SCALE_FACTOR);
    cannonUp.loadImg("data/ArrowUp.png");
    cannonDown = new Button (cux, cdy, cuheilen, cuheilen, 0, fire.c, "", SCALE_FACTOR);
    cannonUp.loadImg("data/ArrowDown.png");
    pouderChargeMore = new Button (pdx, cuY, cuheilen, cuheilen, 0, fire.c, "", SCALE_FACTOR);
    pouderChargeLess = new Button (pdx, cdy, cuheilen, cuheilen, 0, fire.c, "", SCALE_FACTOR);
    switch (site) {
    case LEFT_PLAYER:
      pouderChargeMore.loadImg("data/ArrowRight.png");
      pouderChargeLess.loadImg("data/ArrowLeft.png");
      break;
    case RIGHT_PLAYER:
      pouderChargeMore.loadImg("data/ArrowLeft.png");
      pouderChargeLess.loadImg("data/ArrowRight.png");
      break;
    }
  }
  /**
   *Displays all objects
   */
  void display() {
    field.display();
    fire.display();
    cannon.display();
    cannonUp.display();
    cannonDown.display();
    pouderChargeMore.display();
    pouderChargeLess.display();
    clabel.display();
  }
  /**
   * Checks if one of the buttons is pressed.
   */
  
  float pouderChargeInRange(float _pouderCharge) {
    if (_pouderCharge > pouderChargeMax) {
      _pouderCharge = pouderChargeMax;
    }
    if (_pouderCharge < pouderChargeMin) {
      _pouderCharge = pouderChargeMin;
    }
    return _pouderCharge;
  }
}