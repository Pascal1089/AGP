class PouderChargeLabel {
  float pouderChargeMax;
  float pouderChargeMin;
  float x;
  float y;
  float le;
  float hei;
  float charge;
  PouderChargeLabel (float _x, float _y, float _le, float _hei, float _pouderChargeMax, float _pouderChargeMin) {
    x = _x;
    y = _y;
    le = _le;
    hei = _hei;
    pouderChargeMin = _pouderChargeMin;
    pouderChargeMax = _pouderChargeMax;
  }

  void display() {
    //stroke(0, 0, 0);
    //rectMode(CORNER); 
    //fill(0, 0, 0, 0);
    //rect(x, y, le, hei);
    //noStroke();
    rectMode(CORNER);
    fill(0, 0, 200);
    rect(calcScreenPositionX(scaleMToPixels(x)), calcScreenPositionY(scaleMToPixels(y)), scaleMToPixels(le), - scaleMToPixels(hei * calcCurrentChargeInPercent(charge)));
  }

  float calcCurrentChargeInPercent(float _charge) {
    return     (_charge - pouderChargeMin) / (pouderChargeMax - pouderChargeMin);
  }

  void setCharge(float _charge) {
    this.charge = _charge;
  }
}