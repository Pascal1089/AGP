/*=======================================================================================================================================*/
/* CLASS GROUND */
/*=======================================================================================================================================*/
class Ground {
  float x;
  float y;
  float le;
  float hei;
  float rad;
  int SCALE_FACTOR;
  color c;

  Ground (float _x, float _y, float _length, float _height, float _radius, color _c, int _SCALE_FACTOR) {
    x = _x;
    y = _y;
    le = _length;
    hei = _height;
    rad = _radius;
    c = _c;
    SCALE_FACTOR = _SCALE_FACTOR;
  }
  void display () {
    noStroke();
    rectMode (CENTER);
    fill(c);
    rect (calcScreenPositionX(scaleMToPixels(x)), calcScreenPositionY(scaleMToPixels(y)), scaleMToPixels(le), scaleMToPixels(hei), rad);
  }
  float getX () {
    return x;
  }
  float getY () {
    return y;
  }
  float getLe () {
    return le;
  }
  float getHei () {
    return hei;
  }
  color getC () {
    return c;
  }
  void setC (color _c) {
    this.c = _c;
  }
  /*
*Scales the unit M to pixels.
   */
  float scalePixelsToM (float _m) {

    return _m / SCALE_FACTOR;
  }
}
/*=======================================================================================================================================*/
/* CLASS BUTTONS AND FIELDS */
class ButtonAndFields extends Ground {

  String title;
  float angle;

  ButtonAndFields (float _x, float _y, float _length, float _height, float _radius, color _c, String _title, float _angle, int _SCALE_FACTOR) {
    super (_x, _y, _length, _height, _radius, _c, _SCALE_FACTOR);
    title = _title;
    angle = _angle;
  }
}
/*============================================================================================================================================*/
class Wall extends Ground {
  float c;
  Wall (float _x, float _y, float _length, float _height, float _radius, color _c, int _SCALE_FACTOR) {
    super(_x, _y, _length, _height, _radius, _c, _SCALE_FACTOR);
    c = _c;
  }
}