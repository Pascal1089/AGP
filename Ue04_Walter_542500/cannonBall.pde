class CannonBall extends Ball { //<>//
  float pouderCharge;
  float angle;

  CannonBall(float _x, float _y, float _diameter, float _weight, color _c, float _pouderCharge) {
    super (_x, _y, _diameter, _weight, _c);
    x0 = _x;
    y0 = _y;
    pouderCharge = _pouderCharge;
  }

  void setVx0 (float v ) {
    vX0 = v;
  }
  void setVy0 (float v) {
    vY0 = v;
  }

  void display () {
    if (isOnGround(this)) {
      calcOnGround();
      y = diameter / 2 ;
    } else {
      calcPosition();
    }
    if (hitWall(this, wallRight) || hitWall(this, wallLeft)) {
      x0 = x;
      y0 = y;
      changeDirection();
      move();
    } 
    ellipseMode(CENTER);
    fill(c);
    ellipse (calcScreenPositionX((x)), 
      calcScreenPositionY(scaleMToPixels(y)), 
      scaleMToPixels(diameter), 
      scaleMToPixels(diameter));
  }
}