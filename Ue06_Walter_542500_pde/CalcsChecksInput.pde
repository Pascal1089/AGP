/*Calculations for the game*///<>// //<>// //<>//
/**
 *Scales the unit M to pixels.
 *@param
 *@return
 */
float scaleMToPixels (float _m) {

  return _m * SCALE_FACTOR;
}

/**
 *Scales the unit M to pixels.
 *@param
 *@return
 */
float scalePixelsToM (float _m) {

  return _m / SCALE_FACTOR;
}

/**
 *Calculates the X-position in machine-coordinates.
 *@param x karthesian coordinate from object
 *@return x i coordinate 
 */
float calcScreenPositionX (float _x) {
  return _x+Xi0;
}


void mousePressed() {
  /**
   *Complete mouse-input handling schould be refactored after adding a game-controller class (Player site enums).
   */
  // if (game.allowChanges) {

  if (mouseButton == LEFT) {
    if (game.soundController.soundControlButton.isInRange()) {
      if (game.soundController.soundIsOn) {
        game.soundController.switchSoundOff();
      } else {
        game.soundController.switchSoundOn();
      }
    }

    if (debugModeButton.isInRange()) {
      if (debugModeIsOn) {
        debugModeIsOn = false;
        println("--Debug mode is off Wind is on --");
      } else {
        debugModeIsOn = true;
        println("--Debug mode is on Wind is off --");
      }
    }
    //if (game.firstPlayerTurn) {
    if (playerLeft.fire.isInRange()) {
      playerLeft.cannon.shoot();
    }
    if (playerLeft.cannonUp.isInRange()) {
      playerLeft.cannon.turnCannon(-1);
    }
    if (playerLeft.cannonDown.isInRange()) {
      playerLeft.cannon.turnCannon(+1);
    }
    if (playerLeft.pouderChargeMore.isInRange()) {
      playerLeft.cannon.pouderCharge += 1;
      playerLeft.cannon.pouderCharge = pouderChargeInRange(playerLeft.cannon.pouderCharge);
    } 
    if (playerLeft.pouderChargeLess.isInRange()) {

      playerLeft.cannon.pouderCharge -= 1;
      playerLeft.cannon.pouderCharge = pouderChargeInRange(playerLeft.cannon.pouderCharge);
    }
    //    } else {

    if (playerRight.fire.isInRange()) {
      playerRight.cannon.shoot();
    }

    if (playerRight.cannonUp.isInRange()) {
      playerRight.cannon.turnCannon(+1);
    }

    if (playerRight.cannonDown.isInRange()) {
      playerRight.cannon.turnCannon(-1);
    }

    if (playerRight.pouderChargeMore.isInRange()) {
      playerRight.cannon.pouderCharge += 0.5;
      playerRight.cannon.pouderCharge = pouderChargeInRange(playerRight.cannon.pouderCharge);
    } 
    if (playerRight.pouderChargeLess.isInRange()) {

      playerRight.cannon.pouderCharge -= 0.5;
      playerRight.cannon.pouderCharge = pouderChargeInRange(playerRight.cannon.pouderCharge);
    }
    //   }
    // }
  }
}

/*
 *Calculates the Y-position in machine-coordinates
 *@param
 *@return
 */
float calcScreenPositionY (float _y) {
  return -_y+Yi0;
}
/**
 *Checks if a ball hits a wall.
 */


/**
 *Checks if a ball hits another ball.
 */
boolean hitBall(float _xBallA, float _yBallA, float _radiusBallA, float _xBallB, float _yBallB, float _radiusBallB) {
  //Compares the quadratic relation between the length of the radius of both balls and the distance between the two balls.
  game.biBombMove = _radiusBallA * _radiusBallA + _radiusBallB * _radiusBallB > (_xBallA - _xBallB) * (_xBallA - _xBallB) + (_yBallA -  _yBallB) * (_yBallA -  _yBallB);
  return _radiusBallA * _radiusBallA + _radiusBallB * _radiusBallB >= (_xBallA - _xBallB) * (_xBallA - _xBallB) + (_yBallA -  _yBallB) * (_yBallA -  _yBallB);
}

boolean hashitWallOtherWay (Ball _b, Ground _g) {
  PVector p1 = new PVector (_g.x - _g.le / 2, _g.hei, 0);
  PVector p2 = new PVector (_g.x - _g.le / 2, 0, 0);
  PVector p3 = new PVector (_b.x, _b.y, 0);
  PVector p4 = new PVector (_g.x + _g.le / 2, _g.hei, 0);
  PVector p5 = new PVector (_g.x + _g.le / 2, 0, 0);
  p2.sub(p1); 
  p3.sub(p1);
  p4.sub(p5);
  p5.sub(p3);

  return _b.rad <= p2.cross(p3).div(sqrt(p2.x * p2.x + p2.y * p2.y)).mag() || _b.rad <= p4.cross(p3).div(sqrt(p4.x * p4.x + p4.y * p4.y)).mag() ;
}

/**
 *Returns true if Ball hits a wall.
 */
boolean hashitWallSide(Ball _ball, Ground _rect) {
  float deltaDistance;
  if (_ball.x < _rect.x) {
    float leftBorder = _rect.x - _rect.le / 2;
    if (_rect.x < 0) {
      deltaDistance = leftBorder + Math.abs(_ball.x);
    } else {
      deltaDistance = leftBorder - _ball.x;
    }
  } else {
    float rightBorder = _rect.x + _rect.le / 2;
    deltaDistance = _ball.x - rightBorder;
  }
  return deltaDistance < _ball.rad && _ball.y - _ball.rad < _rect.hei;
}

boolean hasHitWallTop(Ball _ball, Ground _rect) {
  float deltaDistance = _ball.y - _rect.hei;
  println(_ball.y + " " + _rect.hei + " " + deltaDistance + " " + _ball.rad);
  return _ball.rad < deltaDistance && _ball.x < _rect.x + _rect.le / 2 && _ball.x > _rect.x - _rect.le / 2 ;
}
/**
 *@param a is a CannonBall
 *@param b is a Ball
 */
void doElasticCollision(CannonBall a, Ball b) {
  float aX = a.x - b.x  ;
  float aY = a.y - b.y  ;
  float ang = cos((aX*1)/(sqrt(aX*aX+aY*aY)+1));
  float v02 = ((2* a.weight) / ( a.weight + b.weight))*  a.vX * ang;
  game.biBombMove = true;
  game.bigBomb.calculateVxVy(ang, v02);
  game.bigBomb.move();
}

/**
 * Checks if pouderCharche is in Range.
 */
float pouderChargeInRange(float _pouderCharge) {
  if (_pouderCharge > pouderChargeMax) {
    _pouderCharge = pouderChargeMax;
  }
  if (_pouderCharge < pouderChargeMin) {
    _pouderCharge = pouderChargeMin;
  }
  return _pouderCharge;
}

boolean ballIsInsideWindow (float x) {
  return scaleMToPixels(x) >= -Xi0 && scaleMToPixels(x) <= Xi0 ;
}