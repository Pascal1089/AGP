import ddf.minim.*; //<>// //<>//
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import ddf.minim.signals.*;
import ddf.minim.spi.*;
import ddf.minim.ugens.*;


/* //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>//
 *@author Pascal Walter, 542500
 *@date 2015-10-16
 *@version 0.5
 *@comment 
 */
//sound
Minim minim = new Minim(this);
AudioSample explosionSound ;
AudioSample shootSound;



//Game constants
PImage backgroundImg;
static float Xi0; //Also offset.
static float Yi0; //Also offset.
static float g = 9.81;
static float SCALE_FACTOR_TIME;
static float GROUND;
static int SCALE_FACTOR;
static float scoreFieldLength;
static float scoreFieldHeight;
static float Xk0;
static float Yk0;
static float pouderChargeMax;
static float pouderChargeMin;
static float coefizient = 0.5;
static float WEIGHT_BIGBALL = 40;
static float CANNON_WEIGHT = 1000;
static float COEFIZIENT_CANNON = 0.5;
static float CANNONBALL_WEIGHT = 5;
static int frmRte = 30;
static float DELTA_TIME;
static float heighestWindVelocity = 2;
boolean debugModeIsOn = false;
PlayerController playerLeft;
PlayerController playerRight;
static enum playerSite {
  LEFT_PLAYER, RIGHT_PLAYER
};
  static enum PlayerTurn {
  LEFT_PLAYER_TURN, RIGHT_PLAYER_TURN
};
  //Game objects
ScoreField  leftScore;
ScoreField rightScore;
Button leftButton;
Button rightButton;
Button debugModeButton;
Ground ground;
Wall wallLeft;
Wall wallRight;
Ball bigBall;
Cannon cannonLeft;
Cannon cannonRight;
CannonBall testBall;
GameController game;

void setup() {
  frameRate(frmRte);
  DELTA_TIME = 1 / (float)frmRte;
  size(800, 400);
  explosionSound = minim.loadSample("explosion.wav");
  shootSound = minim.loadSample("shoot.wav");
  //Initialisation from constants
  backgroundImg = loadImage("data/Background.png");
  GROUND = height / 100;
  Xi0 = width/2;
  Xk0 = scalePixelsToM(Xi0);
  Yk0 = scalePixelsToM(Yi0);
  Yi0 = height-(height/100)*20;
  SCALE_FACTOR = 20;
  SCALE_FACTOR_TIME = 1000;
  pouderChargeMax = 70;
  pouderChargeMin = 8;
  background(255, 255, 255);
  //Calculation of dimensions from game objects.
  float scoreFieldX = width / 2.5 / 20;
  float scoreFieldY = height / 1.428571429 / 20;
  float buttonLength = (width/SCALE_FACTOR)/10;
  float buttonHeight = ((height/SCALE_FACTOR)/5)/2;
  float fireButtonY = -((height /10) / 20 )  ;
  float wallX = width / SCALE_FACTOR / 4;
  float wallY =  (buttonLength / 2) ;
  float diameter = (width / 10) /SCALE_FACTOR;
  float cannonHeight = height / SCALE_FACTOR / 7 / 2 ;
  float cannonX = (width  / SCALE_FACTOR /3) + 3;
  float cannonY = scalePixelsToM(height/100*5)/2 + cannonHeight + cannonHeight / 2;
  //Initialization of game objects
  wallLeft = new Wall (-wallX, wallY, scalePixelsToM(height/100*5), buttonLength, 0, color(50, 12, 0), SCALE_FACTOR);
  wallRight = new Wall (wallX, wallY, scalePixelsToM(height/100*5), buttonLength, 0, color(50, 12, 0), SCALE_FACTOR);
  leftScore = new ScoreField (-scoreFieldX, scoreFieldY, buttonLength, buttonHeight, 25, color(255, 0, 0), "Score", 0, SCALE_FACTOR);
  rightScore = new ScoreField (scoreFieldX, scoreFieldY, buttonLength, buttonHeight, 25, color(0, 255, 0), "Score", 0, SCALE_FACTOR);

  rightScore.resetScoreField();
  leftScore.resetScoreField();
  //Initialization from cannon left with angle of minus 45° because of coordinate system
  cannonLeft = new Cannon(-cannonX, cannonY, buttonLength, cannonHeight, 0, color (0), SCALE_FACTOR, -45  );
  cannonRight = new Cannon(cannonX, cannonY, buttonLength, cannonHeight, 0, color (0), SCALE_FACTOR, -135  );
  leftButton = new Button (-scoreFieldX, fireButtonY, buttonLength, buttonHeight, 0, color(255, 0, 0), "Fire", SCALE_FACTOR, leftScore, cannonLeft);
  rightButton = new Button (scoreFieldX, fireButtonY, buttonLength, buttonHeight, 0, color(0, 255, 0), "Fire", SCALE_FACTOR, rightScore, cannonRight);
  debugModeButton = new Button(-3,-1,2,2,0,color(0,100,0),"Deb.",SCALE_FACTOR);
  bigBall = new Ball (0, diameter/2 , diameter, WEIGHT_BIGBALL, color(50, 25, 0));

  //Creating players
  playerLeft = new PlayerController (leftScore, leftButton, cannonLeft, playerSite.LEFT_PLAYER);
  playerRight = new PlayerController (rightScore, rightButton, cannonRight, playerSite.RIGHT_PLAYER);
  game = new GameController(playerLeft, playerRight, wallLeft, wallRight, bigBall);
}
/*
*Draws objects on canvas
 */
void draw() {
  //BackgroundImage
  imageMode(CORNER);
  image(backgroundImg, 0, 0);
  game.display();
}