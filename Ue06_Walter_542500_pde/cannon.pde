/* CANNON CLASS *///<>// //<>// //<>//
class Cannon extends Ground {
  PImage   cannonImg = loadImage("Cannon.png");
  PImage lafeteImg;
  float angle;
  float pouderCharge; // meter per second²
  CannonBall b;
  ScoreField field;
  float startAngle; 
  float weight = CANNON_WEIGHT;
  float vX0;
  float vY0;
  float x0;
  float y0;
  float deltaTime;
  float time0;
  boolean hasShooten;
  float resetShootCheckCounter  = 0;
  float xStartPosition;
  float yStartPosition;
  boolean returnCannon = false;
  float vX, vY;
  float recoilx, recoily;
  boolean allowShoot = false;
  float l0 = 1;
  float yPipe;

  Cannon (float _x, float _y, float _length, float _height, float _radius, color _c, int _SCALE_FACTOR, float _angle) {
    super(_x, _y, _length, _height, _radius, _c, _SCALE_FACTOR);
    pouderCharge = (pouderChargeMax + pouderChargeMin) / 2;
    angle = _angle;
    startAngle = _angle;
    x0 = _x;
    y0 = _y;
    yPipe = hei / 2;
    xStartPosition = x0;
    yStartPosition = y0;
    if (xStartPosition < 0) {
      lafeteImg = loadImage("lafete2.png");
    } else {
      lafeteImg = loadImage("lafete.png");
    }
  }

  void display () {
    if (hasShooten) {
      calcPosition();
      recoilHandling();
    }
    drawMySelf();
    showReloadMessage();
    recoilHandling();
  }
  /*
  *Turns the cannon.
   */
  void turnCannon (float _angle) {
    this.angle += _angle;
    //Range for angle 45°-75°
    if (angle <= startAngle - 30) {
      angle = startAngle - 30;
    }
    if (angle > startAngle + 30) { 
      angle = startAngle + 30;
    }
  }
  void shoot () {
    if (x == xStartPosition && b == null && allowShoot) { 
      b = new CannonBall(this.x + le * cos(radians(-angle)), this.y + le * sin(radians(-angle)), hei, CANNONBALL_WEIGHT, color(255, 200, 200), pouderCharge);
      b.calculateVxVy(angle, pouderCharge);
      b.move();
      calcRecoilV();
      time0 = millis();
      hasShooten = true;
      if (game.soundController.soundIsOn) {
        shootSound.trigger();
      }
    }
  }

  void calcRecoilV() {
    float recoilVelociy =  (b.weight /  this.weight) * b.pouderCharge * sqrt(this.weight/(this.weight + b.weight));
    vX = cos(radians(- angle)) * -recoilVelociy;
    vY = sin(radians(- angle)) * -recoilVelociy;
    vX0 = vX;
    vY0 = vY;
  }
  void calcPosition() {
    vY =(-(900/weight)*vY0-1000/weight*(hei / 2 - l0))*DELTA_TIME;
    
    if (x < 0) {
      if (weight * g * deltaTime * deltaTime < abs( vX0 * deltaTime)) {
        vX = vX - (0.5 * COEFIZIENT_CANNON * g  * DELTA_TIME);
      }
    } else {
      if (weight * g * deltaTime * deltaTime < abs( vX0 * deltaTime)) {
        vX = vX + (0.5 * COEFIZIENT_CANNON * g * DELTA_TIME);
        vY = -vY;
        println(vY);
      }
    }
    x = x + vX * DELTA_TIME ;
    yPipe = yPipe + vY + DELTA_TIME;
  }

  void drawMySelf() {
    imageMode(CORNER);
    pushMatrix();
    if (x < 0) {
      translate(calcScreenPositionX(scaleMToPixels(x - le / 10)), calcScreenPositionY(scaleMToPixels(y - hei/2)));
    } else {
      translate(calcScreenPositionX(scaleMToPixels(x + le / 10)), calcScreenPositionY(scaleMToPixels(y - hei/2)));
    }
    rotate(radians(angle));
    image(cannonImg, 0 - scaleMToPixels(le / 10), 0 - scaleMToPixels(hei / 2));
    popMatrix();
    rectMode(CENTER);
    fill(128, 64, 0);
    noStroke();
    if (x > 0) {
      image(lafeteImg, calcScreenPositionX(scaleMToPixels(x - le - hei / 2)), calcScreenPositionY(scaleMToPixels(y + hei / 3)));
    } else {
      image(lafeteImg, calcScreenPositionX(scaleMToPixels(x - hei / 2)), calcScreenPositionY(scaleMToPixels(y + hei / 3)));
    }
  }

  void showReloadMessage() {
    if (returnCannon) {
      fill(0);
      text("RELOADING", calcScreenPositionX(scaleMToPixels(-5)), calcScreenPositionY(scaleMToPixels(10)));
    }
  }

  void recoilHandling() {
    if (hasShooten) {
      resetShootCheckCounter ++;
      if (resetShootCheckCounter>50) {
        hasShooten = false;
        resetShootCheckCounter = 0;
        returnCannon = true;
      }
    }
    if (returnCannon) {
      resetShootCheckCounter ++;
      if (x < 0) {
        x += 0.01;
      } else {
        x -= 0.01;
      }
      if (!(x <= xStartPosition - 0.01 || x >= xStartPosition + 0.01 ) || !(x == xStartPosition)) {
        returnCannon = false;
        resetShootCheckCounter = 0;
        x = xStartPosition;
      }
    }
  }
}