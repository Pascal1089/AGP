/* Ball */ //<>//
class Ball {
  float x;
  float y;
  float diameter;
  float rad;
  float weight;
  color c;
  float time;
  float time0 = 0;
  float vY0 = 0;
  float vX0 = 0;
  float x0;
  float y0;
  float FN;
  float FNR;
  float angle;
  float pouderCharge;
  boolean hitWX;
  float xBefore;
  float yBefore;
  boolean noVelocity = false;
  float vX = 0;
  float vY = 0;
  float t = 0;
  float densityIron = 0.000787;
  boolean isOnGroundCheck = false;
  //r is a part of Strömungsreibung from Newton. r = Cw * p * A
  float r;

  Ball (float _x, float _y, float _diameter, float _weight, color _c) {
    x = _x;
    y = _y;
    x0 = _x;
    y0 = _y;
    weight = (4/3) * PI * (_diameter / 2) * (_diameter / 2) * densityIron;
    diameter = _diameter;
    weight = _weight;
    c = _c;
    rad = diameter / 2;
    FN = weight * g;
    FNR =  weight * g * coefizient;
    r = 0.45 * 1.3 * PI * rad * rad;
  }

  void display () {
    calculateMovementOnGround();
    drawMySelf();
  }

  void drawMySelf() {
    ellipseMode(CENTER);
    fill(c);
    ellipse (calcScreenPositionX(scaleMToPixels(x)), 
      calcScreenPositionY(scaleMToPixels(y)), 
      scaleMToPixels(diameter), 
      scaleMToPixels(diameter));
  }

  void move () {
    display();
    noVelocity = false;
  }

  void calculateVxVy(float _angle, float _pouderCharge) {
    angle = _angle;
    vX = cos(radians(- _angle)) * _pouderCharge;
    vY = sin(radians(- _angle)) * _pouderCharge;
    vX0 = vX;
    vY0 = vY;
  }

  void changeDirectionOnGround() {
    vX = -vX;
    vX0 = vX;
  }

  void calculateMovementOnGround() {
    xBefore = x;
    yBefore = y;
    y = rad;
    if (vX != 0) {
      if ( 0.5 * coefizient * g  * DELTA_TIME < Math.abs(vX) ) {
        if (vX > 0) {
          vX = vX - (0.5 * coefizient * g  * DELTA_TIME);
        } else {  
          vX = vX + (0.5 * coefizient * g  * DELTA_TIME);
        }
        x = x + vX  * DELTA_TIME;
      } else {
        x0 = x;
        noVelocity = true;
      }
    }
  }

  boolean isOnGround() {
    return this.y - this.rad <= 0 ;
  }

  void calculateAngle() {
    float aX = xBefore - x;
    float aY = yBefore - y;
    angle = cos((aX*1)/(sqrt(aX*aX+aY*aY)+1));
  }

  float getX () {
    return x;
  }

  float getY () {
    return y;
  }

  float getDiameter () {
    return diameter;
  }

  color getC () {
    return c;
  }
}