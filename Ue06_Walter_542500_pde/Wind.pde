/**
 *Class Wind 
 *
 */
class Wind {
  float windStrongnessAndDirection;
  float highestWindVelocity = 2;
  float lowestWindStrongnessIncluded = -1;
  //Is 1.1 because upper param high in method random is excluded.
  float highestWindStrongnessExcluded = 1.1;
  float upperLimitForWindStrongness = 1;
  Wind (float _maxWindVelocity) {
    highestWindVelocity = _maxWindVelocity;
  }

  void createWind() {
    windStrongnessAndDirection =  createWindDirectionAndStrongness() * highestWindVelocity;
  }

  float createWindDirectionAndStrongness() {
    float directionAndStrongness = random(lowestWindStrongnessIncluded, highestWindStrongnessExcluded);
    if (directionAndStrongness > upperLimitForWindStrongness) {
      directionAndStrongness = upperLimitForWindStrongness ;
    }
    return directionAndStrongness;
  }
}

class WindLabel {
  float windStrongnessAndDirection;
  float maxWind;
  WindLabel(float _maxWind) {
    maxWind = _maxWind;
  }

  void setWindStrognessAndDirection (float _newStrongnessAndDirection) {
    windStrongnessAndDirection = normalizeStrongness(_newStrongnessAndDirection);
  }

  float normalizeStrongness (float _param) {
    return (_param / maxWind) * 10;
  }

  void display() {

    fill(0);
    triangle(calcScreenPositionX(scaleMToPixels(Math.round(windStrongnessAndDirection))), calcScreenPositionY(275)
      , calcScreenPositionX(0), calcScreenPositionY(250)
      , calcScreenPositionX(0), calcScreenPositionY(300));
    fill(255, 0, 0);
    textSize(15);
    text(Math.abs(Math.round(windStrongnessAndDirection)), calcScreenPositionX(windStrongnessAndDirection), calcScreenPositionY(225));
  }
}