/*=======================================================================================================================================*/
/*Class PlayerController*/
class PlayerController {
  //import processing.sound*;

  ScoreField field;
  Button fire;
  Cannon cannon;
  Button cannonUp;
  Button cannonDown;
  Button pouderChargeMore;
  Button pouderChargeLess;
  PouderChargeLabel clabel;
  Ground[] objects;
  float pouderCharge = pouderChargeMax + pouderChargeMin / 2;
  playerSite site;
  boolean ballIsOnGround = false;
  float explosionX, explosionY;
  boolean triggerExplosion = false;
  Explosion explosion;
  int explosionCounter = 0;
  boolean   explosionCheck = false;

  PlayerController(ScoreField _field, Button _button, Cannon _cannon, playerSite _site) {//, Button _cannonUp, Button _cannonDown, Button _pouderChargeMore, Button _pouderChargeLess) {
    site = _site;
    field = _field;
    fire = _button;
    cannon = _cannon;
    float cux, pdx, cuY, cdy;
    float cuheilen = fire.hei / 2;
    float offset = 2;
    cuY = fire.y + 0.1 + cuheilen / 2 ;
    cdy = fire.y - 0.1 - cuheilen / 2 ;
    if (fire.x > 0) {
      cux = fire.x  - fire.le / 2 - offset;
      pdx = fire.x - 0.1 - fire.le / 2 - cuheilen - offset;
    } else {
      cux = fire.x  + fire.le / 2 + offset;
      pdx = fire.x + 0.1 + fire.le / 2 + cuheilen + offset;
    }

    cannonUp = new Button (cux, cuY, cuheilen, cuheilen, 0, fire.c, "", SCALE_FACTOR);
    cannonUp.loadImg("data/ArrowUp.png");
    cannonDown = new Button (cux, cdy, cuheilen, cuheilen, 0, fire.c, "", SCALE_FACTOR);
    cannonDown.loadImg("data/ArrowDown.png");
    pouderChargeMore = new Button (pdx, cuY, cuheilen, cuheilen, 0, fire.c, "", SCALE_FACTOR);
    pouderChargeLess = new Button (pdx, cdy, cuheilen, cuheilen, 0, fire.c, "", SCALE_FACTOR);
    switch (site) {
    case LEFT_PLAYER:
      pouderChargeMore.loadImg("data/ArrowRight.png");
      pouderChargeLess.loadImg("data/ArrowLeft.png");
      clabel = new PouderChargeLabel(pdx + cuheilen / 2 + 0.2, cdy - cuheilen / 2, cuheilen, cuheilen + cuheilen + 0.22, pouderChargeMax, pouderChargeMin);
      break;
    case RIGHT_PLAYER:
      pouderChargeMore.loadImg("data/ArrowLeft.png");
      pouderChargeLess.loadImg("data/ArrowRight.png");
      clabel = new PouderChargeLabel(pdx - cuheilen - cuheilen / 2 - 0.2, cdy - cuheilen / 2, cuheilen, cuheilen + cuheilen + 0.22, pouderChargeMax, pouderChargeMin);
      break;
    }

    clabel.setCharge(cannon.pouderCharge);
  }
  /**
   *Displays all objects
   */
  void display() {
    field.display();
    fire.display();
    cannon.display();
    cannonUp.display();
    cannonDown.display();
    pouderChargeMore.display();
    pouderChargeLess.display();
    clabel.setCharge(cannon.pouderCharge);
    clabel.display();

    //Handle cannon ball
    if (cannon.b != null) {
      //Display the CannonBall 
      cannon.b.display();
      if (cannon.b.isOnGround()) {
        if (!ballIsOnGround) {
          ballIsOnGround = true;
        }
        if (cannon.b.noVelocity) {
          explosion = new Explosion (cannon.b.x, cannon.b.y, cannon.b.diameter);
          triggerExplosion = true;
          cannon.b = null;
          if (game.soundController.soundIsOn) {
            explosionSound.trigger();
          }
        }
      }
      if ( cannon.b != null && (hitBall(cannon.b.x, cannon.b.y, cannon.b.rad, bigBall.x, bigBall.y, bigBall.rad)) ) { 
        triggerExplosion = true;
        explosion = new Explosion (cannon.b.x, cannon.b.y, cannon.b.diameter);
        doElasticCollision(cannon.b, game.bigBomb);
        cannon.b = null;
        if (game.soundController.soundIsOn) {
          explosionSound.trigger();
        }
      }
      if (cannon.b != null && !ballIsInsideWindow(cannon.b.x)) {
        triggerExplosion = true;
        explosion = new Explosion (cannon.b.x, cannon.b.y, cannon.b.diameter);
        cannon.b = null;
        if (game.soundController.soundIsOn) {
          explosionSound.trigger();
        }
      }
    }
    if (triggerExplosion) {
      if (explosionCounter < 6) {
        explosion.display();
        explosionCounter ++;
      } else {
        triggerExplosion = false;
        explosion = null;
        explosionCounter = 0;
      }
    }
  }
}
/*######################################################################################################################################*/
class PouderChargeLabel {
  float pouderChargeMax;
  float pouderChargeMin;
  float x;
  float y;
  float le;
  float hei;
  float charge;
  PouderChargeLabel (float _x, float _y, float _le, float _hei, float _pouderChargeMax, float _pouderChargeMin) {
    x = _x;
    y = _y;
    le = _le;
    hei = _hei;
    pouderChargeMin = _pouderChargeMin;
    pouderChargeMax = _pouderChargeMax;
  }

  void display() {
    rectMode(CORNER);
    fill(0, 0, 200);
    rect(calcScreenPositionX(scaleMToPixels(x)), calcScreenPositionY(scaleMToPixels(y)), scaleMToPixels(le), - scaleMToPixels(hei * calcCurrentChargeInPercent(charge)));
  }

  float calcCurrentChargeInPercent(float _charge) {
    return     (_charge - pouderChargeMin) / (pouderChargeMax - pouderChargeMin);
  }

  void setCharge(float _charge) {
    this.charge = _charge;
  }
}