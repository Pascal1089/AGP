/* BUTTON CLASS*/
class Button extends Ground {
  ScoreField field;
  Cannon cannon;
  String title;
  color hoverColor;
  color c;
  PImage buttonImage ;

  Button(float _x, float _y, float _length, float _height, float _radius, color _c, String _title, int _SCALE_FACTOR, ScoreField _field, Cannon _cannon) {
    super( _x, _y, _length, _height, _radius, _c, _SCALE_FACTOR);
    title = _title;
    field = _field;
    hoverColor = c + color (125);
    c = _c;
    cannon = _cannon;
    cannon.field = this.field;
  }
  Button(float _x, float _y, float _length, float _height, float _radius, color _c, String _title, int _SCALE_FACTOR) {
    super( _x, _y, _length, _height, _radius, _c, _SCALE_FACTOR);
    title = _title;
    hoverColor = _c + color (125);
    c = _c;
  }
  Button(float _x, float _y, float _length, float _height,int _SCALE_FACTOR){
    super(_x,_y,_length,_height,0,color(255,255,255,255),_SCALE_FACTOR);
    title = "";
  
  }

  boolean isInRange() {
    boolean inRange = false;
    if (mouseX <= calcScreenPositionX(scaleMToPixels(x + le/2)) 
      && mouseX >= calcScreenPositionX(scaleMToPixels(x - le/2)) 
      && mouseY >= calcScreenPositionY(scaleMToPixels(y + hei/2)) 
      && mouseY <= calcScreenPositionY(scaleMToPixels(y - hei/2))) {
      inRange = true;
    }
    return inRange;
  }

  void display () {
    changeColorIfMouseHoovers();
    super.display();
    showButtonImage();
    writeButtonText();
  }

  void loadImg(String _pathToImg) {
    buttonImage = loadImage(_pathToImg);
  }

  void changeColorIfMouseHoovers () {
    if (isInRange()) {
      super.c = hoverColor;
    } else {
      super.c = c;
    }
  }

  void showButtonImage () {
    if (buttonImage != null) {
      image(buttonImage, calcScreenPositionX(scaleMToPixels(x - le / 2)), calcScreenPositionY(scaleMToPixels(y + hei / 2)));
    }
  }

  void writeButtonText () {
    fill(0);
    textSize(30);
    text (title, calcScreenPositionX(scaleMToPixels(x-super.le/4)), calcScreenPositionY(scaleMToPixels(y-super.hei/3)) );
    if (field != null) {
      field.display();
    }
  }
}