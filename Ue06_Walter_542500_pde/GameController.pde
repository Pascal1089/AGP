/**
 *Gamecontroller class controlles the course of the game.
 *@author Pascal Walter 
 *@version 0.1
 */
class GameController {
  SoundController soundController;
  PlayerController left;
  PlayerController right;
  Wall wallLeft;
  Wall wallRight;
  Ball bigBomb;
  PlayerTurn turn;
  Wind wind = new Wind(heighestWindVelocity);
  WindLabel windLabel = new WindLabel(heighestWindVelocity);
  //First play is always left.
  boolean firstPlayerTurn = true;
  boolean startSequence = true;
  boolean endSequence = false;
  boolean playersTurnEnd = false;
  boolean allowLeftPlayerShoot = true;
  boolean allowRightPlayerShoot = false;
  boolean biBombMove = false;
  boolean hasShooten = false;
  boolean resetBigBomb = false;
  boolean allowChanges = false;
  boolean triggerExplosion = false;
  boolean displayExplosion = false;
  boolean setWind = true;
  float storeWind = 0;
  int timeCounter = 0;
  Explosion explosion;


  GameController (PlayerController _left, PlayerController _right, Wall _wallLeft, Wall _wallRight, Ball _bigBall) {
    left = _left;
    right = _right;
    wallLeft = _wallLeft;
    wallRight = _wallRight;
    bigBomb = _bigBall;
    soundController = new SoundController();
  }

  void display () {
    windLabel.display();
    left.display();
    right.display();
    wallLeft.display();
    wallRight.display();
    bigBomb.display();
    soundController.soundControlButton.display();

    if (setWind) {
      wind.createWind();
      setWind = false;
      storeWind = wind.windStrongnessAndDirection;
      windLabel.setWindStrognessAndDirection(storeWind);
    }
    if (timeCounter <= 90) {
      String s = "";
      if (firstPlayerTurn) {
        s = "left player";
      } else {
        s = "right player";
      }
      text(s, calcScreenPositionX(scaleMToPixels(-4)), calcScreenPositionY(scaleMToPixels(6)));      
      timeCounter++;
    } else {
      if (firstPlayerTurn && allowLeftPlayerShoot && !allowRightPlayerShoot) {
        //Left (first) player´s turn

        if (!hasShooten) {
          left.cannon.allowShoot = true;
          allowChanges = true;
        }
        if (left.cannon.allowShoot && left.cannon.b != null) {
          left.cannon.allowShoot = false;
          hasShooten = true;
        }
      } else if (!firstPlayerTurn && !allowLeftPlayerShoot && allowRightPlayerShoot) {
        //Right (second) player´s turn
        if (!hasShooten) {
          right.cannon.allowShoot = true;
          allowChanges = true;
        }
        if (right.cannon.allowShoot && right.cannon.b != null) {
          right.cannon.allowShoot = false;
          allowChanges = false;
          hasShooten = true;
          allowChanges = false;
        }
      } else {
        //fall back error message if settings in firstPlayerTurn and the allowShoot *  variables are wrong.
        println("-- error permissions to shoot and player selection set wrong --");
      }
    }
    //BigBomb handling here
    if (biBombMove) {
      if (bigBomb.noVelocity) {
        bigBomb.vX0 = 0;
        bigBomb.vY0 = 0;
        biBombMove = false;
      }
      if (firstPlayerTurn) {
        if (hashitWallSide(bigBomb, wallLeft)) {
          left.field.decrement();
          resetBigBomb = true;
          triggerExplosion = true;
        }
        if (hashitWallSide(bigBomb, wallRight)) {
          left.field.increScore();
          resetBigBomb = true;
          triggerExplosion = true;
        }
        timeCounter = 0;
      } else {
        if (hashitWallSide(bigBomb, wallRight)) {
          left.field.decrement();
          resetBigBomb = true;
          triggerExplosion = true;
        }
        if (hashitWallSide(bigBomb, wallLeft)) {
          left.field.increScore();
          resetBigBomb = true;
          triggerExplosion = true;
        }
      }
    }
    if (triggerExplosion) {
      explosion = new Explosion(bigBomb.x, bigBomb.y, bigBomb.diameter);
      triggerExplosion = false;
      displayExplosion = true;
    }
    if (timeCounter<100 && displayExplosion) {
      explosion.display();
      timeCounter++;
      if (timeCounter <=100) {
        explosion = null;
        timeCounter = 0;
        displayExplosion = false;
      }
    }
    // A players turn is over if the cannon ball is exploded and he has shooten and the BigBomb does not move anymore
    if ((left.cannon.b == null && right.cannon.b == null) && hasShooten && !biBombMove && !triggerExplosion) {
      playersTurnEnd = true;
    }
    if (playersTurnEnd) {
      endRound();
    }
  }

  /**
   *Sets up permissions for shooting and players
   */
  void endRound() {
    //TODO: switches Player allow nextPlayer shoot and changeFirstplayerTurn.
    hasShooten = false;
    playersTurnEnd = false;
    setWind = true;
    timeCounter = 0;
    if (resetBigBomb) {
      bigBomb.x = 0;
      bigBomb.y = bigBomb.rad;
    }
    if (firstPlayerTurn) {
      firstPlayerTurn = false;
      allowRightPlayerShoot = true;
      allowLeftPlayerShoot = false;
    } else {
      firstPlayerTurn = true; 
      allowLeftPlayerShoot = true;
      allowRightPlayerShoot = false;
    }
  }
}