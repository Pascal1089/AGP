class CannonBall extends Ball { //<>// //<>//
  float pouderCharge;
  float angle;
  float wind = game.storeWind;
  boolean hitWY = false;
  CannonBall(float _x, float _y, float _diameter, float _weight, color _c, float _pouderCharge) {
    super (_x, _y, _diameter, _weight, _c);
    x0 = _x;
    y0 = _y;
    pouderCharge = _pouderCharge;
  }

  void setVx0 (float v ) {
    vX0 = v;
  }
  void setVy0 (float v) {
    vY0 = v;
  }

  void display () {
    xBefore = x;
    yBefore = y;
    if (!isOnGroundCheck) {
      calcPosition();
    }
    if (isOnGround()) {  
      if (!isOnGroundCheck) {
        if (!debugModeIsOn) {
          vX = vX + wind;
        }
        x0 = x;
        y0 = y;
        vY = 0;
        isOnGroundCheck = true;
      }
    }
    if (hashitWallSide(this, wallRight) || hashitWallSide(this, wallLeft)) {
      hitWX = true;
    }
    if (hitWX) {
      x0 = x;
      y0 = y;
      if (isOnGroundCheck) {
        changeDirectionOnGround();
      } else {
        changeDirectionX();
      }
      hitWX = false;
    }
    /*
    if (hasHitWallTop(this, wallRight) || hasHitWallTop(this, wallLeft)) {
      hitWY = true;
      println("top " + y);
    }
    if (hitWY) {
      x0 = x;
      y0 = y;
      changeDirectionY();
      hitWY = false;
    }
*/
    if (isOnGroundCheck) {
      calculateMovementOnGround();
    }
    drawMySelf();
  }

  void calcPosition () {
    t = t + DELTA_TIME;
    //
    float vXOld = vX;
    float vYOld = vY;
    vX = vXOld - (( r/weight) * vXOld * sqrt(vXOld*vXOld + vYOld * vYOld)) * DELTA_TIME;
    if (!debugModeIsOn) {
      x = x + (vX + wind) * DELTA_TIME;
    } else {
      // x = x + (vX + wind) * DELTA_TIME;
      x = x + vX * DELTA_TIME;
    }
    vY = vYOld - (g + (r/weight) * vYOld * sqrt(vXOld*vXOld + vYOld * vYOld)) * DELTA_TIME;
    y = y + vY * DELTA_TIME;
  }
  void changeDirectionX() {

    vX = -vX;
    vX0 = vX;
  }

  void changeDirectionY() {
    vY = -vY;
    vY0 = vY;
  }
  void calculateMovementOnGround () {
    xBefore = x;
    yBefore = y;
    y = rad;
    if (vX != 0) {
      if ( 0.5 * coefizient * g  * DELTA_TIME < Math.abs(vX) ) {
        if (vX > 0) {
          vX = vX - ( coefizient * g  * DELTA_TIME);
        } else {
          vX = vX + (  coefizient * g  * DELTA_TIME);
        }
        x = x + vX   * DELTA_TIME;
      } else {
        x0 = x;
        noVelocity = true;
      }
    }
  }
}