class SoundController {
  boolean soundIsOn = false;
  Button soundControlButton;
  SoundController() {
    soundControlButton = new Button(0, -2, 2, 2,SCALE_FACTOR);
    soundControlButton.loadImg("SoundOff.png");
  }

  void switchSoundOn() {
    soundControlButton.loadImg("SoundOn.png");
    soundIsOn = true;
  }

  void switchSoundOff() {
    soundControlButton.loadImg("SoundOff.png");
    soundIsOn = false;
  }
}