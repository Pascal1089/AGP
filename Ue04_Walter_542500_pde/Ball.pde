/*=============================================================================================================================*/
/* Ball */
class Ball {
  float x;
  float y;
  float diameter;
  float rad;
  float weight;
  color c;
  float deltaTime = 0;
  float time;
  float time0 = 0;
  float vY0 = 0;
  float vY;
  float vX0 = 0;
  float x0;
  float y0;
  float FN;
  float FNR;
  float angle;
  float pouderCharge;
  boolean hitW;
  float xBefore;
  float yBefore;
  boolean noVelocity = false;

  boolean isOnGroundCheck = false;
  Ball (float _x, float _y, float _diameter, float _weight, color _c) {
    x = _x;
    y = _y;
    x0 = _x;
    y0 = _y;
    weight = (4/3)*PI*(_diameter / 2) * (_diameter / 2) * 0.000787;
    diameter = _diameter;
    weight = _weight;
    c = _c;
    rad = diameter / 2;
    FN = weight * g;
    FNR =  weight * g * coefizient;
  }

  void display () {
    xBefore = x;
    yBefore = y;
    calcPosition();
    if (isOnGround()) {  
      if (!isOnGroundCheck) {
        resetTime0();
        x0 = x;
        y0 = y;
        vY0 = 0;
        isOnGroundCheck = true;
      }
    }
    if (hitWall(this, wallRight) || hitWall(this, wallLeft)) {
      hitW = true;
    }
    if (hitW) {
      resetTime0();
      x0 = x;
      y0 = y;
      changeDirection();
      hitW = false;
    }
    if (isOnGroundCheck) {
      calcOnGround();
      y = diameter / 2 ;
    }

    ellipseMode(CENTER);
    fill(c);
    ellipse (calcScreenPositionX(scaleMToPixels(x)), 
      calcScreenPositionY(scaleMToPixels(y)), 
      scaleMToPixels(diameter), 
      scaleMToPixels(diameter));
  }
  /*
   *Starts the movement of the cannon ball.
   */
  void move () {
    resetTime0();
    display();
  }
  void calcPosition () {
    deltaTime = (millis() - time0) / SCALE_FACTOR_TIME;
    x = x0 + vX0 * deltaTime;
    y = y0 + vY0 *deltaTime - (g*(deltaTime*deltaTime))/2;
  }

  /**
   *Calculates vX0 and vY0 with an angle and a force.
   */
  void calculateVxVy(float _angle, float _pouderCharge) {
    angle = _angle;
    vX0 = cos(radians(- _angle)) * _pouderCharge;
    vY0 = sin(radians(- _angle)) * _pouderCharge;
  }
  /**
   *Changes direction from the ball if ball hits obsticle.
   */
  void changeDirection() {
    calculateAngle();
    vX0 = cos(radians(angle)) * pouderCharge;
  }
  /**
   *Calculates the x coordinate if ball hits ground.
   */
  void calcOnGround() {
    deltaTime = (millis() - time0) / SCALE_FACTOR_TIME;
    if (vX0 != 0) {
      if (vX0 > 0) {
        if ( 0 <= vX0 - coefizient * g * deltaTime) {
          x = x0 + vX0 * deltaTime - 0.5 * coefizient * g  * deltaTime * deltaTime;// + (weight  * g * deltaTime * deltaTime /2);
          print(noVelocity);
        } else {
          noVelocity = true;
        }
      } else {
        if (0 >= vX0 + coefizient * g * deltaTime) {
          x = x0 + vX0 * deltaTime + 0.5 * coefizient * g  * deltaTime * deltaTime;
        } else {
          noVelocity = true;
        }
      }
    }
  }
  /**
   *Checks if Ball is on the ground.
   */
  boolean isOnGround() {
    if (this.y - this.diameter / 2 <= 0) {
      if (!isOnGroundCheck) {
        resetTime0();
      }
      return true;
    }
    return false;
  }

  void resetTime0 () {
    time0 = millis();
  }

  /*
  *Scales the unit M to pixels.
   */
  float scaleMToPixels (float _m) {
    return _m * SCALE_FACTOR;
  }

  float getX () {
    return x;
  }

  float getY () {
    return y;
  }

  float getDiameter () {
    return diameter;
  }

  color getC () {
    return c;
  }

  void calculateAngle() {
    float aX = xBefore - x;
    float aY = yBefore - y;
    angle = cos((aX*1)/(sqrt(aX*aX+aY*aY)+1));
  }
}