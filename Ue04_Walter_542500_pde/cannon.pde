/*===========================================================================================================================*/ //<>//
/* CANNON CLASS */
class Cannon extends Ground {
  PImage   cannonImg = loadImage("Cannon.png");
  PImage lafeteImg;
  float angle;
  float pouderCharge; // meter per second²
  CannonBall b;
  ScoreField field;
  float startAngle; 
  float weight = CANNON_WEIGHT;
  float explosionX, explosionY;
  boolean triggerExplosion = false;
  Explosion explosion;
  int explosionCounter = 0;
  float vX0;
  float vY0;
  float x0;
  float y0;
  float deltaTime;
  float time0;
  boolean shootCheck;
  float resetSootCheckCounter  = 0;
  float xStartPosition;
  float yStartPosition;
  boolean returnCannon = false;
  float countDownBallIsOnGround = 0;
  boolean ballIsOnGround = false;
  float recoilx, recoily;
  Cannon (float _x, float _y, float _length, float _height, float _radius, color _c, int _SCALE_FACTOR, float _angle) {
    super(_x, _y, _length, _height, _radius, _c, _SCALE_FACTOR);

    pouderCharge = (pouderChargeMax + pouderChargeMin) / 2;
    angle = _angle;
    startAngle = _angle;
    x0 = _x;
    y0 = _y;
    xStartPosition = x0;
    yStartPosition = y0;
    if (x < 0) {
      lafeteImg = loadImage("lafete2.png");
    } else {
      lafeteImg = loadImage("lafete.png");
    }
  }

  void display () {
    imageMode(CORNER);
    if (shootCheck) {
      calcPosition();
    }
    // Draw cannon.
    pushMatrix();
    if (x < 0) {
      translate(calcScreenPositionX(scaleMToPixels(x - le / 10)), calcScreenPositionY(scaleMToPixels(y - hei/2)));
    } else {
      translate(calcScreenPositionX(scaleMToPixels(x + le / 10)), calcScreenPositionY(scaleMToPixels(y - hei/2)));
    }
    rotate(radians(angle));
    image(cannonImg, 0 - scaleMToPixels(le / 10), 0 - scaleMToPixels(hei / 2));
    popMatrix();
    rectMode(CENTER);
    fill(128, 64, 0);
    noStroke();
    if (x > 0) {
      image(lafeteImg, calcScreenPositionX(scaleMToPixels(x - le - hei / 2)), calcScreenPositionY(scaleMToPixels(y + hei / 3)));
    } else {
      image(lafeteImg, calcScreenPositionX(scaleMToPixels(x - hei / 2)), calcScreenPositionY(scaleMToPixels(y + hei / 3)));
    }
    //REALOAD massage.
    if (returnCannon) {
      //Info for User that he cant shoot.
      fill(0);
      text("RELOADING", calcScreenPositionX(scaleMToPixels(-5)), calcScreenPositionY(scaleMToPixels(10)));
    }
    //Handle cannon ball
    if (b != null) {
      //Display the CannonBall 
      b.display();

      if (b.isOnGround()) {
        if (!ballIsOnGround) {
          ballIsOnGround = true;
          countDownBallIsOnGround = millis();
        }
        if (b.noVelocity) {
          explosion = new Explosion (b.x, b.y, b.diameter);
          triggerExplosion = true;
          b = null;
        }
      }
      if ( b != null && (hitBall(b.x, b.y, b.diameter / 2, bigBall.x, bigBall.y, bigBall.diameter/2)) ) { 
        this.field.increScore();
        triggerExplosion = true;
        explosion = new Explosion (b.x, b.y, b.diameter);
        b = null;
      }
    }
    if (triggerExplosion) {
      if (explosionCounter < 6) {
        explosion.display();
        explosionCounter ++;
      } else {
        triggerExplosion = false;
        explosion = null;
        explosionCounter = 0;
      }
    }

    if (shootCheck) {
      resetSootCheckCounter ++;
      if (resetSootCheckCounter>50) {
        shootCheck = false;
        resetSootCheckCounter = 0;
        returnCannon = true;
      }
    }

    if (returnCannon) {
      resetSootCheckCounter ++;
      if (x < 0) {
        x += 0.01;
      } else {
        x -= 0.01;
      }
      if (!(x <= xStartPosition - 0.01 || x >= xStartPosition + 0.01 )) {
        returnCannon = false;
        resetSootCheckCounter = 0;
        x = xStartPosition;
      }
    }
  }
  /*
  *Turns the cannon.
   */
  void turnCannon (float _angle) {
    this.angle += _angle;
    //Range for angle 45°-75°
    if (angle <= startAngle - 30) {
      angle = startAngle - 30;
    }
    if (angle > startAngle + 30) { 
      angle = startAngle + 30;
    }
  }
  void shoot () {
    //TODO: Cannon shoot cannon-ball related to angle and powder charge.
    if (x == xStartPosition && b == null) {
      b = new CannonBall(this.x + le * cos(radians(-angle)), this.y + le * sin(radians(-angle)), hei, 10, color(255, 200, 200), pouderCharge);
      b.calculateVxVy(angle, pouderCharge);
      b.move();
      calcRecoilV();
      time0 = millis();
      shootCheck = true;
    }
  }
  void calcRecoilV() {
    float recoilVelociy =  (b.weight /  this.weight) * b.pouderCharge * sqrt(this.weight/(this.weight + b.weight));
    calculateVxVy( angle, - recoilVelociy);
  }

  void calculateVxVy(float _angle, float _pouderCharge) {
    vX0 = cos(radians(- _angle)) * _pouderCharge;
    vY0 = sin(radians(- _angle)) * _pouderCharge;
  }
  void calcPosition() {
    deltaTime = (millis() - time0) / SCALE_FACTOR_TIME;
    if (x < 0) {
      if (weight * g * deltaTime * deltaTime < abs( vX0 * deltaTime)) {
        x = x0 + vX0 * deltaTime + (weight * g * deltaTime * deltaTime) / 2;// + (weight  * g * deltaTime * deltaTime /2);
      }
    } else {
      if (weight * g * deltaTime * deltaTime < abs( vX0 * deltaTime)) {
        x = x0 + vX0 * deltaTime - (weight * g * deltaTime * deltaTime) / 2;
      }
    }
  }
}