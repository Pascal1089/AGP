/* //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>//
 *@author Pascal Walter, 542500
 *@date 2015-10-16
 *@version 0.4
 *@comment 
 */

//Game constants
PImage backgroundImg;
static float Xi0; //Also offset.
static float Yi0; //Also offset.
static float g = 9.81;
static float SCALE_FACTOR_TIME;
static float GROUND;
static int SCALE_FACTOR;
static float scoreFieldLength;
static float scoreFieldHeight;
static float Xk0;
static float Yk0;
static float pouderChargeMax;
static float pouderChargeMin;
static float coefizient = 0.5;
PlayerController playerLeft;
PlayerController playerRight;
static enum playerSite {
  LEFT_PLAYER, RIGHT_PLAYER
  
};
//Changed Cannon-weight.
static float CANNON_WEIGHT = 5;

//SoundFile shoot = new SoundFile(this,"data/SHOOT.wav");
//SoundFile explosion = new SoundFile( this,"data/explosion.wav");
//Game objects
ScoreField  leftScore;
ScoreField rightScore;
Button leftButton;
Button rightButton;
Ground ground;
Wall wallLeft;
Wall wallRight;
Ball bigBall;
Cannon cannonLeft;
Cannon cannonRight;
CannonBall testBall;

void setup() {
  size(800, 400);
  //Initialisation from constants
  backgroundImg = loadImage("data/Background.png");
  GROUND = height / 100;
  Xi0 = width/2;
  Xk0 = scalePixelsToM(Xi0);
  Yk0 = scalePixelsToM(Yi0);
  Yi0 = height-(height/100)*20;
  SCALE_FACTOR = 20;
  SCALE_FACTOR_TIME = 1000;
  pouderChargeMax = 14.5;
  pouderChargeMin = 8;
  background(255, 255, 255);
  //Calculation of dimensions from game objects.
  float scoreFieldX = width / 2.5 / 20;
  float scoreFieldY = height / 1.428571429 / 20;
  float buttonLength = (width/SCALE_FACTOR)/10;
  float buttonHeight = ((height/SCALE_FACTOR)/5)/2;
  float fireButtonY = -((height /10) / 20 )  ;
  float wallX = width / SCALE_FACTOR / 4;
  float wallY =  (buttonLength / 2) ;
  float diameter = (width / 10) /SCALE_FACTOR;
  float cannonHeight = height / SCALE_FACTOR / 7 / 2 ;
  float cannonX = (width  / SCALE_FACTOR /3) + 3;
  float cannonY = scalePixelsToM(height/100*5)/2 + cannonHeight + cannonHeight / 2;
  //Initialization of game objects
  wallLeft = new Wall (-wallX, wallY, scalePixelsToM(height/100*5), buttonLength, 0, color(50, 12, 0), SCALE_FACTOR);
  wallRight = new Wall (wallX, wallY, scalePixelsToM(height/100*5), buttonLength, 0, color(50, 12, 0), SCALE_FACTOR);
  leftScore = new ScoreField (-scoreFieldX, scoreFieldY, buttonLength, buttonHeight, 25, color(255, 0, 0), "Score", 0, SCALE_FACTOR);
  rightScore = new ScoreField (scoreFieldX, scoreFieldY, buttonLength, buttonHeight, 25, color(0, 255, 0), "Score", 0, SCALE_FACTOR);
  //ground = new Ground(0.0, 0.0, scalePixelsToM(width), scalePixelsToM(height/100*5), 0, color(0,106, 29, 2), SCALE_FACTOR);

  rightScore.resetScoreField();
  leftScore.resetScoreField();
  //Initialization from cannon left with angle of minus 45° because of coordinate system
  cannonLeft = new Cannon(-cannonX, cannonY, buttonLength, cannonHeight, 0, color (0), SCALE_FACTOR, -45  );
  cannonRight = new Cannon(cannonX, cannonY, buttonLength, cannonHeight, 0, color (0), SCALE_FACTOR, -135  );
  leftButton = new Button (-scoreFieldX, fireButtonY, buttonLength, buttonHeight, 0, color(255, 0, 0), "Fire", SCALE_FACTOR, leftScore, cannonLeft);
  rightButton = new Button (scoreFieldX, fireButtonY, buttonLength, buttonHeight, 0, color(0, 255, 0), "Fire", SCALE_FACTOR, rightScore, cannonRight);
  bigBall = new Ball (0, diameter/2, diameter, 100, color(50, 25, 0));

  //Creating players
  playerLeft = new PlayerController (leftScore, leftButton, cannonLeft, playerSite.LEFT_PLAYER);
  playerRight = new PlayerController (rightScore, rightButton, cannonRight, playerSite.RIGHT_PLAYER);
  
  
}
/*
*Draws objects on canvas
 */
void draw() {
  background(255);
  imageMode(CORNER);
  image(backgroundImg, 0, 0);
  // ground.display();
  wallLeft.display();
  wallRight.display();
  bigBall.display();
  playerLeft.display();
  playerRight.display();
}